import React, { Component } from 'react'
import { svgToJson } from './svgToJson'

const styles = {
  wrapper: {
    padding: '0 20%',
    height: '300px',
  },
  textArea: {
    width: '100%',
    height: '300px',
  }
}



class App extends Component {
  state = {
    svgText: '',
    svgJson: 'Paste svg string above and click \'render json\' button'
  }

  onChange = (event) => {
    this.setState({ svgText: event.target.value })
  }

  renderSvgJson = () => {
    const parser = new DOMParser()
    const svgDoc = parser.parseFromString(this.state.svgText, 'image/svg+xml')
    const json = svgToJson(svgDoc)
    this.setState({ svgJson: json })
  }
  
  render() {
    return (
      <div style={styles.wrapper}>
        <h2>Convert SVG string to JSON for asset server</h2>
        <textarea
          style={styles.textArea}
          onChange={this.onChange}
          value={this.state.svgText}
        />
        <button onClick={this.renderSvgJson}>Render json</button>
        <div className="App">
          <pre>{this.state.svgJson}</pre>
        </div>
      </div>
    )
  }
}

export default App;
