const getAttrsObjects = (doc) => {
  const allAttrs = []
  if (!doc.attributes) return null
  for (let attr of doc.attributes) {
    const attribute = {
      name: attr.nodeName,
      value: attr.nodeValue,
    }
    allAttrs.push(attribute)
  }
  return allAttrs
}

const buildSvgObject = (objects) => {
  console.log('objects: ', objects)
  const svgObj = {
    svg: {

    }
  }

  // get paths
  const paths = objects.filter((obj) => obj.name === 'path')
  svgObj.svg.path = paths.map((path) => {
    const d = path.attributes.find((attr) => attr.name === 'd')
    return {
      d: d.value
    }
  })

  // get viewBox
  const svg = objects.find((obj) => obj.name === 'svg')
  const viewBox = svg.attributes.find((attr) => attr.name === 'viewBox')
  svgObj.svg.viewBox = viewBox.value

  return svgObj
}

export const svgToJson = (document) => {
  const allObjects = []
  const loopThroughNodes = (doc) => {
    const obj = {
      name: doc.nodeName,
      attributes: getAttrsObjects(doc),
    }
    allObjects.push(obj)
    if (doc.children.length === 0) return
    for (let child of doc.children) {
      loopThroughNodes(child)
    }
  } 
  loopThroughNodes(document)
  const svgObject = buildSvgObject(allObjects)
  return JSON.stringify(svgObject, null, 2)
}